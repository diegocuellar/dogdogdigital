﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UITest : UIView {

    void Start()
    {
        this.ShowView();
    }
    protected override void Initialize()
    {
        base.Initialize();
        gameObject.SetActive(true);
    }

    protected override void DeInitialize()
    {
        gameObject.SetActive(false);
    }

    public void OnClosePanel()
    {
        CloseView();
    }
}

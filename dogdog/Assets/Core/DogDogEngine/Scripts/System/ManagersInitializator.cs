﻿using UnityEngine;
using System.Collections;

public class ManagersInitializator : MonoBehaviour {

    private static bool _initialized = false;

    void Awake()
    {
        if(_initialized == false)
        {
            GameObject managers = (GameObject)Resources.Load("GameManager");
            GameObject instance = GameObject.Instantiate(managers, new Vector3(0, 0, -5), Quaternion.identity) as GameObject;
            instance.name = "GameManager";
            DontDestroyOnLoad(instance);
            _initialized = true;
        }
    }

    void Start()
    {
        Destroy(gameObject);
        Debug.Log("Iniciado");
    }
}

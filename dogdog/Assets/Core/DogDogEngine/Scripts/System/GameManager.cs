﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum GameCursor
{
    Pointer,
    Interact,
    Dialog
}

public class GameManager : MonoBehaviour {
    protected static GameManager _instance;
    protected static GameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();
            return _instance;
        }
    }

    protected static Hashtable _globalVariables;
    protected static Hashtable GlobalVariables
    {
        get
        {
            if (_globalVariables == null)
                _globalVariables = new Hashtable();
            return _globalVariables;
        }
        set { _globalVariables = value;  }
    }

    void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        Application.targetFrameRate = 60;
        Cursor.visible = !Application.isMobilePlatform;
    }

    public static void SetGlobalVar<T>(string name, T value)
    {
        if (GlobalVariables.ContainsKey(name))
            GlobalVariables[name] = value;
        else
            GlobalVariables.Add(name, value);
    }
}

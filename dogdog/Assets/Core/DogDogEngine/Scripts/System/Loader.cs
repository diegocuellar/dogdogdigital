﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;
using System.Collections.Generic;

using UnityEngine.EventSystems;


public class Loader : MonoBehaviour {

    private static Loader _instance;
    public CanvasGroup Group;
    public Slider ProgressBar;

    protected static Stack<string> Navigation;
    public static Stack<string> NavigationStack
    {
        get { return Navigation; }
    }

    void Awake()
    {
        _instance = this;
        Navigation = new Stack<string>();
    }

    public static void Reload()
    {
        _instance.StartCoroutine(_instance.Load(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name));
    }

    public static void GoBack()
    {
        if(Navigation.Count > 0)
        {
            if(Navigation.Peek() == UnityEngine.SceneManagement.SceneManager.GetActiveScene().name)
            {
                Navigation.Pop();
                GoBack();
            }
            else
            {
                GameManager.SetGlobalVar<bool>("GoBack", true);
                _instance.StartCoroutine(_instance.Load(Navigation.Pop()));
            }
        }
    }

    public static void LoadLevel(string lvl, bool navigation = true)
    {
        if(navigation)
        {
            Navigation.Push(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        }
        _instance.StartCoroutine(_instance.Load(lvl));
    }

    protected IEnumerator Load(string lvl)
    {
        EventSystem e = FindObjectOfType<EventSystem>();
        if (e != null)
            e.enabled = false;
        float t = 0;
        while (t<1)
        {
            t += Time.deltaTime / 0.25f;
            Group.alpha = Mathf.Lerp(0, 1, t);
            yield return 0;
        }

        AsyncOperation ao = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(lvl);

        while (!ao.isDone)
        {
            yield return ao.isDone;
            ProgressBar.value = ao.progress;
        }
        ProgressBar.value = 1;
        yield return ao;

        System.GC.Collect();
        ao = Resources.UnloadUnusedAssets();
        yield return ao;

        yield return new WaitForSeconds(0.1f);

        t = 0;
        while(t<1)
        {
            t += Time.deltaTime / 0.25f;
            Group.alpha = Mathf.Lerp(1, 0, t);
            yield return 0;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class UIView : MonoBehaviour {

    public delegate IEnumerator AnimationCoroutine(UIView target);

    public RectTransform CacheTransform;
    public GameObject CacheGameObject;
    public CanvasGroup Group;
    protected Animator ViewAnimator;
    protected Coroutine ActiveRoutine;
    protected Coroutine ActiveAnimation;

    protected virtual void Awake()
    {
        CacheTransform = transform as RectTransform;
        CacheGameObject = gameObject;
        Group = GetComponent<CanvasGroup>();
        ViewAnimator = GetComponent<Animator>();
        if (ViewAnimator == null)
            ViewAnimator = CacheGameObject.AddComponent<Animator>();
    }

    protected virtual void Initialize() { }

    protected virtual void OnDisplay() { }

    protected virtual void OnClose() { }

    protected virtual void DeInitialize() { }

    protected IEnumerator ShowCoroutine(AnimationCoroutine anim)
    {
        if(anim != null)
        {
            ActiveAnimation = StartCoroutine(anim(this));
            yield return ActiveAnimation;
        }

        if (ViewAnimator.HasState(Animator.StringToHash("Base Layer.Showview"), 0))
        {
            while (ViewAnimator.GetCurrentAnimatorStateInfo(0).fullPathHash != Animator.StringToHash("Base Layer.Showview")) yield return 0;

            while (ViewAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1) yield return 0;
        }
        ViewAnimator.SetTrigger("Idle");
        OnDisplay();
    }

    protected IEnumerator CloseCoroutine(AnimationCoroutine anim)
    {
        if (anim != null)
        {
            ActiveAnimation = StartCoroutine(anim(this));
            yield return ActiveAnimation;
        }

        if (ViewAnimator.HasState(Animator.StringToHash("Base Layer.CloseView"), 0))
        {
            while (ViewAnimator.GetCurrentAnimatorStateInfo(0).fullPathHash != Animator.StringToHash("Base Layer.CloseView")) yield return 0;

            while (ViewAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1) yield return 0;
        }
        ViewAnimator.SetTrigger("Exit");
        DeInitialize();
    }
    
    public void ShowView(AnimationCoroutine anim = null)
    {
        Initialize();
        if(gameObject.activeSelf)
        {
            if (ActiveRoutine != null)
                StopCoroutine(ActiveRoutine);
            if (ActiveAnimation != null)
                StopCoroutine(ActiveAnimation);
            ViewAnimator.SetTrigger("Show");
            ActiveRoutine = StartCoroutine(ShowCoroutine(anim));
        }
    }

    public void CloseView(AnimationCoroutine anim = null)
    {
        OnClose();
        if(gameObject.activeSelf)
        {
            if (ActiveRoutine != null)
                StopCoroutine(ActiveRoutine);
            if (ActiveAnimation != null)
                StopCoroutine(ActiveAnimation);
            ViewAnimator.SetTrigger("Close");
            ActiveRoutine = StartCoroutine(CloseCoroutine(anim));
        }
    }
}
